package config

import (
	"io/ioutil"
	"gopkg.in/yaml.v2"
)

func LoadConfig(configFile string, config interface{}) error{
	configMem, err := ioutil.ReadFile(configFile)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(configMem, config)

	if err != nil {
		return err
	}
	return nil
}
