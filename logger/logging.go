package logger

import (
	"fmt"
	"io"
	"log"
	"os"
)

const (
	TraceColor   = "\033[1;32m[TRACE]   \033[0m"
	InfoColor    = "\033[1;30m[INFO]    \033[0m"
	NoticeColor  = "\033[1;36m[NOTICE]  \033[0m"
	WarningColor = "\033[1;33m[WARNING] \033[0m"
	ErrorColor   = "\033[1;31m[ERROR]   \033[0m"
	DebugColor   = "\033[0;35m[DEBUG]   \033[0m"
)

var (
	tra  *Logger
	inf  *Logger
	not  *Logger
	warn *Logger
	erro *Logger
	deb  *Logger
)

type FuncLogger func(int) *Logger
type lOut struct {
	tra  Logger
	inf  Logger
	not  Logger
	warn Logger
	erro Logger
	deb  Logger
}

type Out struct {
	Trace   FuncLogger
	Info    FuncLogger
	Notice  FuncLogger
	Warning FuncLogger
	Error   FuncLogger
	Debug   FuncLogger
}

func newL(out io.Writer, prefix string, flag int, logLevel int) *Logger {
	return &Logger{logg: log.New(out, prefix, flag), suffix: "", logLevel: 100, globalLevel: logLevel}
}

type Logger struct {
	logg        *log.Logger
	suffix      string
	logLevel    int
	globalLevel int
}

func (logg *Logger) loggerFunc(level int) *Logger {
	return &Logger{logg: logg.logg, suffix: logg.suffix, logLevel: level, globalLevel: logg.globalLevel}
}

func Init(logLevel int, debugLevel int, logHandler io.Writer, debugHandler io.Writer) *Out {
	var o Out
	if logHandler == nil {
		logHandler = os.Stdout
	}
	if debugHandler == nil {
		debugHandler = os.Stdout
	}

	var logFlag int
	if debugLevel > 0 {
		logFlag = log.Ldate | log.Ltime | log.Lshortfile
	} else if logLevel > 2 {
		logFlag = log.Ldate | log.Ltime
	}

	inf = newL(logHandler, InfoColor, logFlag, logLevel)
	o.Info = inf.loggerFunc
	tra = newL(debugHandler, TraceColor, logFlag, logLevel)
	o.Trace = tra.loggerFunc
	not = newL(logHandler, NoticeColor, logFlag, logLevel)
	o.Notice = not.loggerFunc
	warn = newL(logHandler, WarningColor, logFlag, logLevel)
	o.Warning = warn.loggerFunc
	erro = newL(logHandler, ErrorColor, logFlag, logLevel)
	o.Error = erro.loggerFunc
	deb = newL(debugHandler, DebugColor, logFlag, debugLevel)
	o.Debug = deb.loggerFunc
	return &o
}

func InitScopeLog() *lOut {
	var o lOut

	o.inf = *inf
	o.tra = *tra
	o.not = *not
	o.warn = *warn
	o.erro = *erro
	o.deb = *deb

	return &o
}

func (l *lOut) AddSuffix(a ...string) *Out {
	var o Out
	var vl string
	for _, v := range a {
		v = "[" + fmt.Sprintf("\033[0;30m%s\033[0m", v) + "]"
		vl = vl + fmt.Sprintf("%s ", v)
	}

	l.inf.suffix = l.inf.suffix + vl
	o.Info = l.inf.loggerFunc

	l.tra.suffix = l.tra.suffix + vl
	o.Trace = l.tra.loggerFunc

	l.not.suffix = l.not.suffix + vl
	o.Notice = l.not.loggerFunc

	l.warn.suffix = l.warn.suffix + vl
	o.Warning = l.warn.loggerFunc

	l.erro.suffix = l.erro.suffix + vl
	o.Error = l.erro.loggerFunc

	l.deb.suffix = l.deb.suffix + vl
	o.Debug = l.deb.loggerFunc

	return &o
}

func (l *Logger) Printf(format string, a ...interface{}) {
	if l.logLevel < l.globalLevel {
		_ = l.logg.Output(2, l.suffix+fmt.Sprintf(format, a...))
	}
}

func (l *Logger) Println(a ...interface{}) {
	if l.logLevel < l.globalLevel {
		_ = l.logg.Output(2, l.suffix+fmt.Sprintln(a...))
	}
}

func (l *Logger) Fatalf(a ...interface{}) {
	_ = l.logg.Output(2, l.suffix+fmt.Sprint(a...))
	os.Exit(1)
}

func (l *Logger) Fatal(a ...interface{}) {
	_ = l.logg.Output(2, l.suffix+fmt.Sprint(a...))
	os.Exit(1)
}

// >>>>>


