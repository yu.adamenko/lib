# logger

You must add in code:

   - in global scope:

            var l *log.Out
        
   - in main/Init (or in simple) function:

			debugLevel = 7
			loggingLevel = 3

			l = log.Init(loggingLevel, debugLevel, f, f)

            // f - descriptor (*os.File) outputt log file, default (/dev/stdout) 
            // loggingLevel - (int) maximum log level
            // debugLevel - (int) maximum debug level (only Debug)
        
How it use in code:

    a := "Test variable" 
	// Info, Trace, Debug, Error, Notice, Warning

    l.Info(2).Printf("Something data: %s\n", a) // 2<3 - displayed
    l.Debug(8).Printf("Something data: %s\n", a) // 8>7 - not not displayed

	

    
    
