package myhttp

import (
	"log"
	"net"
	"net/http"
)

// GetIPAdress Getting an IP address from http header,
func GetIPAdress(r *http.Request) string {
	if ipProxy := r.Header.Get("X-FORWARDED-FOR"); len(ipProxy) > 0 {
		return ipProxy
	}
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	return ip
}

// CheckIPAccess ... security module
func CheckIPAccess(testedIP *string, testingNET *string) bool {
	_, IPnet, err := net.ParseCIDR(*testingNET)
	if err != nil {
		log.Println(err)
		return false
	}
	return IPnet.Contains(net.ParseIP(*testedIP))
}

// ChecksIPAccess ...
func ChecksIPAccess(testedIP *string, testingNETs *[]string) bool {
	for _, net := range *testingNETs {
		if CheckIPAccess(testedIP, &net) {
			return true
		}
	}
	return false
}

// StringIP ...
type StringIP string

func (testedIP *StringIP) ResultCheckIP(deny *[]string, alow *[]string) bool {
	ip := string(*testedIP)
	if ChecksIPAccess(&ip, deny) {
		return false
	}
	return ChecksIPAccess(&ip, alow)
}
