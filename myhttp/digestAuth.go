package myhttp

import (
	"net/http"
	"time"
	"io"
	"bytes"
	"io/ioutil"
	"strings"
	"crypto/md5"
	"encoding/hex"
	"crypto/rand"
	"fmt"
)

type HttpStruct struct {
	Uri 		string
	Method 		string
	Username 	string
	Password 	string
	Timeout		time.Duration
}

func ReqHttpDigestAuth(httpData HttpStruct, postBody *[]byte) (int,error) {
	req, err := http.NewRequest(httpData.Method, httpData.Uri, nil)
	req.Header.Set("Content-Type", "application/json")
	timeout := httpData.Timeout * time.Second
	client := &http.Client{Timeout: timeout}
	resp, err := client.Do(req)

	if resp != nil && err == nil {
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusUnauthorized {
			return resp.StatusCode, nil
		}
		digestParts := DigestParts(resp)
		digestParts["uri"] = httpData.Uri
		digestParts["method"] = httpData.Method
		digestParts["username"] = httpData.Username
		digestParts["password"] = httpData.Password
		var body io.Reader
		if postBody != nil {
			body = bytes.NewBuffer(*postBody)
		}
		req, err = http.NewRequest(httpData.Method, httpData.Uri, body)
		req.Header.Set("Authorization", GetDigestAuthrization(digestParts))
		req.Header.Set("Content-Type", "application/json")

		resp, err = client.Do(req)
		if err != nil {
			return 500, err
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			_, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return 500, err
			}
		}
		return resp.StatusCode, nil
	}
	return 500, err
}

func DigestParts(resp *http.Response) map[string]string {
	result := map[string]string{}
	if len(resp.Header["Www-Authenticate"]) > 0 {
		wantedHeaders := []string{"nonce", "realm", "qop"}
		responseHeaders := strings.Split(resp.Header["Www-Authenticate"][0], ",")
		for _, r := range responseHeaders {
			for _, w := range wantedHeaders {
				if strings.Contains(r, w) {
					result[w] = strings.Split(r, `"`)[1]
				}
			}
		}
	}
	return result
}

func sGetMD5(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func getHexMD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func GetCnonce() string {
	b := make([]byte, 8)
	io.ReadFull(rand.Reader, b)
	return fmt.Sprintf("%x", b)[:16]
}

func GetDigestAuthrization(digestParts map[string]string) string {
	d := digestParts
	ha1 := getHexMD5Hash(d["username"] + ":" + d["realm"] + ":" + d["password"])
	ha2 := getHexMD5Hash(d["method"] + ":" + d["uri"])
	nonceCount := 00000001
	cnonce := GetCnonce()
	response := getHexMD5Hash(fmt.Sprintf("%s:%s:%v:%s:%s:%s", ha1, d["nonce"], nonceCount, cnonce, d["qop"], ha2))
	authorization := fmt.Sprintf(`Digest username="%s", realm="%s", nonce="%s", uri="%s", cnonce="%s", nc="%v", qop="%s", response="%s"`,
		d["username"], d["realm"], d["nonce"], d["uri"], cnonce, nonceCount, d["qop"], response)
	return authorization
}

