package main

import (
	"database/sql"
	"fmt"
	"git.blackbee.com.ua/adam/lib/ormsql"
	"github.com/cockroachdb/cockroach/pkg/util/uuid"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type AuthRegistrationRequest struct {
	AppId                string   `protobuf:"bytes,1,opt,name=appId,proto3" json:"appId,omitempty"`
	Mobile               string   `protobuf:"bytes,2,opt,name=mobile,proto3" json:"mobile,omitempty"`
	Code                 string   `protobuf:"bytes,3,opt,name=code,proto3" json:"code,omitempty"`
	Key                  string   `protobuf:"bytes,4,opt,name=key,proto3" json:"key,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

var arr = AuthRegistrationRequest{AppId: "123", Mobile: "0503103167", Code: "87342873", Key: "simsimopen"}
var arr2 AuthRegistrationRequest
var EmptyString = ""

var config = ormsql.SQLSet{
	Server:      "192.168.14.65",
	Port:        "26257",
	NameDB:      "navcolo",
	UserDB:      "root",
	Ssl:         true,
	SslMode:     "verify-full",
	SslRootCert: "/Users/yuriyadam/GoglandProjects/lib/ormsql/test/cert/ca.crt",
	SslKey:      "/Users/yuriyadam/GoglandProjects/lib/ormsql/test/cert/client.root.key",
	SslCert:     "/Users/yuriyadam/GoglandProjects/lib/ormsql/test/cert/client.root.crt",
	//Protocol:    "",
	//PassDB:      "",
}

func main() {
	type person struct {
		Mobile     string `sqlIns:"mobile" sqlUpt:"mobile"`
		TimeSecret int64  `sqlIns:"TimeSecret" sqlUpt:"TimeSecret"`
		TimeCreate int64  `sqlIns:"TimeCreate"`
		TimeChange int64  `sqlUpt:"TimeChange"`
	}

	type retdata struct {
		Id string
		//Kr int64
	}
	fmt.Println(">>",uuid.MakeV4())
	TimeSecret := time.Now().UnixNano()

	var personData = person{
		Mobile:     "380503103166",
		TimeSecret: TimeSecret + 2,
		TimeCreate: TimeSecret + 3,
		TimeChange: TimeSecret + 4,
	}
	//var personDataGet person

	var retId retdata

	db, err := ormsql.New(&config).PgSQL()
	if err != nil {
		fmt.Println("1", err)
		return
	}
/*
	err = db.InsertOrUpdate(&personData, "persons", "(mobile)", "", "sqlIns", "sqlUpt")
	if err != nil {
		fmt.Println("2", err)
		return
	}
*/
	err = db.InsertReturn(&personData, "persons", "sqlIns", &retId,"")
	if err != nil {
		fmt.Println("2", err)
		return
	}
	fmt.Println(retId)

/*
	err = InsertReturn(&personData, "persons", "sqlIns", &retId,"")
	if err != nil {
		fmt.Println("2", err)
		return
	}
*/

/*
	_, err = db.SelectToOne(&personDataGet, "persons", "mobile = '380503103167'", "")
	if err != nil {
		fmt.Println("3", err)
		return
	}
	fmt.Println(">>>", personDataGet)
*/
}

func Insert(value interface{}, table string, tagName string) error {
	const MySql = "mysql"
	const PGSql = "postgres"

	type object struct {
		TypeDB string
	}
	var obj = object{
		"postgres",
	}
	var sqlVariable string
	insertValues := "INSERT INTO " + table + " ("
	valueQMStr := " VALUES ("
	var updateStr string
	var updateStrVar string

	switch obj.TypeDB {
	case MySql: //for Mysql
		updateStr = " ON DUPLICATE KEY UPDATE "
	case PGSql:
		updateStr = " ON CONFLICT ("
	}

	v := reflect.ValueOf(value)

	structS := v.Elem()
	lenStruct := structS.NumField()
	values := make([]interface{}, lenStruct)

	k := 0
	l := lenStruct

	for i := 0; i < lenStruct; i++ {
		if tagName == EmptyString {
			_, flag := structS.Type().Field(i).Tag.Lookup("sql")
			if flag {
				sqlVariable = structS.Type().Field(i).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(structS.Type().Field(i).Name)
			}

		} else {
			sqlVariable = structS.Type().Field(i).Tag.Get(tagName) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if sqlVariable == EmptyString {
			l = l - 1
			values = values[:l]
		} else {
			insertValues = insertValues + sqlVariable + ","

			switch obj.TypeDB {
			case MySql: //for Mysql
				valueQMStr = valueQMStr + "?,"
				updateStr = updateStr + sqlVariable + "=?,"
			case PGSql:
				valueQMStr = valueQMStr + "$" + strconv.Itoa(i+1) + ","
				updateStrVar = updateStrVar + sqlVariable + "=$" + strconv.Itoa(i+1) + ","
				updateStr = updateStr + sqlVariable + ","
			}

			values[k] = structS.Field(i).Interface()
			k += 1
		}
	}

	for _, r := range values {
		values = append(values, r)
	}

	var insertUpdateValues string
	switch obj.TypeDB {
	case MySql: //for Mysql
		insertUpdateValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ")" + updateStr[:len(updateStr)-1]
	case PGSql:
		insertUpdateValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ")" + updateStr[:len(updateStr)-1] + ") DO UPDATE SET " + updateStrVar[:len(updateStrVar)-1]
	}

	fmt.Println(insertUpdateValues)
	return nil
}

func InsertOrUpdate(value interface{}, table string, clause string, condition string, tagInsert string, tagUpdate string) error {
	var sqlInsVariable string
	var sqlUpdVariable string

	insertValues := "INSERT INTO " + table + " ("
	valueQMStr := " VALUES ("
	var updateStrVar string
	updateStr := " ON CONFLICT "

	v := reflect.ValueOf(value)

	structS := v.Elem()
	lenStruct := structS.NumField()
	values := make([]interface{}, lenStruct)
	//valuesUpt := make([]interface{}, lenStruct)
	//var values []interface{}

	k := 0
	l := lenStruct

	for i := 0; i < lenStruct; i++ {
		if tagInsert == EmptyString {
			_, flag := structS.Type().Field(i).Tag.Lookup("sql")
			if flag {
				sqlInsVariable = structS.Type().Field(i).Tag.Get("sql")
			} else {
				sqlInsVariable = strings.ToLower(structS.Type().Field(i).Name)
			}

		} else {
			sqlInsVariable = structS.Type().Field(i).Tag.Get(tagInsert) // returns the tag string
		}
		if tagUpdate != EmptyString {
			sqlUpdVariable = structS.Type().Field(i).Tag.Get(tagUpdate)
		}

		if strings.Contains(sqlInsVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlInsVariable, ",")
			sqlInsVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if strings.Contains(sqlUpdVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlUpdVariable, ",")
			sqlUpdVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		fmt.Println(sqlUpdVariable)

		if sqlInsVariable == EmptyString && sqlUpdVariable == EmptyString {
			l = l - 1
			values = values[:l]
		} else {
			values[k] = structS.Field(i).Interface()
			k += 1
		}

		if sqlInsVariable != EmptyString {
			insertValues = insertValues + sqlInsVariable + ","
			valueQMStr = valueQMStr + "$" + strconv.Itoa(i+1) + ","
		}

		if sqlUpdVariable != EmptyString {
			updateStrVar = updateStrVar + sqlUpdVariable + "=$" + strconv.Itoa(i+1) + ","
		}
		fmt.Println(values)
	}

	var insertUpdateValues string

	insertUpdateValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ")" + updateStr + clause + " DO UPDATE SET " + updateStrVar[:len(updateStrVar)-1] + " " + condition

	fmt.Println(insertUpdateValues)
	fmt.Println(values)
	return nil
}

func InsertReturn(value interface{}, table string, additionalTag string, returning interface{}, additionalRetTag string,) error {
	insertValues := "INSERT INTO " + table + " ("
	valueQMStr := " VALUES ("
	t := reflect.ValueOf(value).Elem()

	lenStruct := t.NumField()
	values := make([]interface{}, lenStruct)
	k := 0
	l := lenStruct
	var sqlInsertVariable string
	for npc := 0; npc < lenStruct; npc++ {
		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlInsertVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlInsertVariable = strings.ToLower(t.Type().Field(npc).Name)
			}

		} else {
			sqlInsertVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlInsertVariable, ",") {
			sqlInsertVariables := strings.SplitAfter(sqlInsertVariable, ",")
			sqlInsertVariable = sqlInsertVariables[0][:len(sqlInsertVariables[0])-1]
		}

		if sqlInsertVariable == "" {
			l = l - 1
			values = values[:l]
		} else {
			insertValues = insertValues + sqlInsertVariable + ","
			values[k] = t.Field(npc).Interface()
			k += 1
			typeOfValues := t.Type().Field(npc).Type.Name()
			switch typeOfValues {
			case "string":
				valueQMStr = valueQMStr + t.Field(npc).String() + ","
			case "int":
				valueQMStr = valueQMStr + strconv.Itoa(int(t.Field(npc).Int())) + ","
			case "int32":
				valueQMStr = valueQMStr + strconv.Itoa(int(t.Field(npc).Int())) + ","
			case "int64":
				valueQMStr = valueQMStr + strconv.Itoa(int(t.Field(npc).Int())) + ","
			case "float32":
				valueQMStr = valueQMStr + fmt.Sprintf("%f", t.Field(npc).Float()) + ","
			case "float64":
				valueQMStr = valueQMStr + fmt.Sprintf("%f", t.Field(npc).Float()) + ","
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
		}
	}
	insertValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ") RETURNING "

	fmt.Println("[1]>>", insertValues)

	rt := reflect.ValueOf(returning).Elem()
	lenRetStruct := rt.NumField()

	valsString := make([]sql.NullString, lenRetStruct)   // Allocate enough values
	valsInt64 := make([]sql.NullInt64, lenRetStruct)     // Allocate enough values
	valsFloat64 := make([]sql.NullFloat64, lenRetStruct) // Allocate enough values
	ints := make([]interface{}, lenRetStruct)            // Allocate middle values
	intsBool := make([]bool, lenRetStruct)

	//count := 0
	k = 0
	//l := lenRetStruct
	var sqlRetVariable,sqlRetString string
	for npc := 0; npc < lenRetStruct; npc++ { // iterates through every struct type field

		if additionalRetTag == EmptyString {
			_, flag := rt.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlRetVariable = rt.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlRetVariable = strings.ToLower(rt.Type().Field(npc).Name)
			}

		} else {
			sqlRetVariable = rt.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlRetVariable, ",") {
			sqlRetVariables := strings.SplitAfter(sqlRetVariable, ",")
			sqlRetVariable = sqlRetVariables[0][:len(sqlRetVariables[0])-1]
		}

		if sqlRetVariable == "" {
			l = l - 1
			valsInt64 = valsInt64[:l]
			valsFloat64 = valsFloat64[:l]
			valsString = valsString[:l]
			ints = ints[:l]
		} else {

			typeOfValues := rt.Type().Field(npc).Type.Name()
			intsBool[npc] = true
			//fmt.Println("Type: ",typeOfValues) //
			switch typeOfValues {
			case "string":
				ints[k] = &valsString[k] // Copy references into the middle values slice
			case "int64":
				ints[k] = &valsInt64[k]
			case "float32":
				ints[k] = &valsFloat64[k]
			case "float64":
				ints[k] = &valsFloat64[k]
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
			k += 1
		}
		sqlRetString =  sqlRetString + "," + sqlRetVariable
	}

	sqlString := insertValues + sqlRetString[1:]


	fmt.Println(sqlString)
	//fmt.Println(values)
	/*
		stmt, err := obj.DB.Prepare(insertValues)
		if err != nil {
			return err
		}
		defer stmt.Close()

		_, err2 := stmt.Exec(values...)
		if err2 != nil {
			return err2
		}
	*/

	/*
		rows, err := obj.DB.Query(sqlString)

			if err != nil {
				return count, err
			} else {
				for rows.Next() {
					count += 1
					err = rows.Scan(ints...)
					if err != nil {
						return count, err
					}
					k = 0
					for i := 0; i < lenStruct; i++ {
						v := t.Field(i)
						if intsBool[i] {
							switch ints[k].(type) {
							case *sql.NullString:
								//fmt.Println("Type: string", )
								if valsString[k].Valid {
									v.SetString(valsString[k].String) // Copy not NULL values into variables of struct
								} else {
									v.SetString("")
								}
							case *sql.NullInt64:
								//fmt.Println("Type: int", )
								if valsInt64[k].Valid {
									v.SetInt(valsInt64[k].Int64)
								} else {
									v.SetInt(0)
								}
							case *sql.NullFloat64:
								//fmt.Println("Type: float", )
								if valsFloat64[k].Valid {
									v.SetFloat(valsFloat64[k].Float64)
								} else {
									v.SetFloat(0)
								}
							default:
								//log.Printf("Error UNKNOWN type: %T", ints[i])
								return fmt.Errorf("Error UNKNOWN type: %T\n", ints[i])
							}
							k++
						}
					}
				}
			}
			return nil
*/
		return nil
}

func SelectToOne(value interface{}, from string, where string, additionalTag string) (int, error) {
	//count := 0
	endSimbol := ","
	selectString := "SELECT "
	t := reflect.ValueOf(value).Elem()
	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct)   // Allocate enough values
	valsInt64 := make([]sql.NullInt64, lenStruct)     // Allocate enough values
	valsFloat64 := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	ints := make([]interface{}, lenStruct)            // Allocate middle values
	intsBool := make([]bool, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable string
	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field

		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(t.Type().Field(npc).Name)
			}

		} else {
			sqlVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if npc == lenStruct {
			endSimbol = ""

			if selectString[len(selectString)-1] == ',' {
				selectString = selectString[:len(selectString)-1] + endSimbol
			}
		}

		if sqlVariable == "" {
			l = l - 1
			valsInt64 = valsInt64[:l]
			valsFloat64 = valsFloat64[:l]
			valsString = valsString[:l]
			ints = ints[:l]
		} else {
			selectString = selectString + sqlVariable + endSimbol

			typeOfValues := t.Type().Field(npc).Type.Name()
			intsBool[npc] = true
			//fmt.Println("Type: ",typeOfValues) //
			switch typeOfValues {
			case "string":
				ints[k] = &valsString[k] // Copy references into the middle values slice
			case "int64":
				ints[k] = &valsInt64[k]
			case "float32":
				ints[k] = &valsFloat64[k]
			case "float64":
				ints[k] = &valsFloat64[k]
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return 0, fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
			k += 1
		}
	}
	if where != "" {
		selectString = selectString[:len(selectString)-1] + " FROM " + from + " WHERE " + where
	} else {
		selectString = selectString[:len(selectString)-1] + " FROM " + from
	}

	return 0, nil
}
