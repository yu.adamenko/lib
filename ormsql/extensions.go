package ormsql

import (
	"database/sql/driver"
	"github.com/google/uuid"
)


// ---- Add types for reflect ----
// NullUUID can be used with the standard sql package to represent a
// UUID value that can be NULL in the database

type NullUUID struct {
	UUID  uuid.UUID
	Valid bool
}

// Value implements the driver.Valuer interface.
func (u NullUUID) Value() (driver.Value, error) {
	if !u.Valid {
		return nil, nil
	}
	// Delegate to UUID Value function
	return u.UUID.Value()
}

func (u *NullUUID) Scan(src interface{}) error {
	if src == nil {
		u.UUID, u.Valid = uuid.UUID{}, false
		return nil
	}

	// Delegate to UUID Scan function
	u.Valid = true

	return u.UUID.Scan(src)
}

