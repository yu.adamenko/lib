package ormsql

import (
	"database/sql"
	log "gitlab.com/yu.adamenko/lib/logger"
	"os"
)

type SQLSet struct {
	TypeDB       string
	Server       string
	Port         string
	Protocol     string
	NameDB       string
	UserDB       string
	PassDB       string
	Ssl          bool
	SslMode      string
	SslRootCert  string
	SslKey       string
	SslCert      string
	LoggingLevel int
	DebugLevel   int
}

const (
	EmptyString = ""
	MySql       = "mysql"
	PGSql       = "postgres"
	SQLite      = "sqlite"
)

var logging = log.Init(9, 9, os.Stdout, os.Stdout)

type SQLObject struct {
	config *SQLSet
}

type DbType interface {
	PgSQL() (*PgSQLObject, error)
	MySQL() (*MySQLObject, error)
}

type MySQLObject struct {
	DB *sql.DB
}

func (obj SQLObject) MySQL() (*MySQLObject, error) {
	var (
		db  *sql.DB
		err error
	)

	db, err = sql.Open(MySql, obj.config.UserDB+":"+obj.config.PassDB+"@"+obj.config.Protocol+"("+obj.config.Server+":"+obj.config.Port+")/"+obj.config.NameDB+"?charset=utf8")

	if err != nil {
		return nil, err
	}

	return &MySQLObject{
		db,
	}, nil
}

type PgSQLObject struct {
	DB *sql.DB
}

func (obj SQLObject) PgSQL() (*PgSQLObject, error) {
	var (
		db  *sql.DB
		err error
	)

	if obj.config.Ssl {
		db, err = sql.Open(PGSql,
			"postgresql://"+obj.config.UserDB+"@"+obj.config.Server+":"+obj.config.Port+"/"+obj.config.NameDB+"?"+
				"ssl=true&"+
				"sslmode="+obj.config.SslMode+"&"+
				"sslrootcert="+obj.config.SslRootCert+"&"+
				"sslkey="+obj.config.SslKey+"&"+
				"sslcert="+obj.config.SslCert)
	} else {
		db, err = sql.Open(PGSql, "user="+obj.config.UserDB+" password="+obj.config.PassDB+" dbname="+obj.config.NameDB+" host="+obj.config.Server+" port="+obj.config.Port+" sslmode=disable")
	}

	if err != nil {
		return nil, err
	}
	return &PgSQLObject{
		db,
	}, nil
}

func New(config *SQLSet) DbType {
	log.Init(config.LoggingLevel, config.DebugLevel, nil, nil)
	return &SQLObject{
		config,
	}
}
