// Copyright (C) 2018 Adamenko Yuriy <yu.adamenko@gmail.com>
//

package ormsql

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"reflect"
	"strings"
)

func (obj MySQLObject) CountRows(table string, where string) (string, error) {
	var countRows string
	countSelect := "SELECT COUNT(*) FROM " + table
	if where != "" {
		countSelect = countSelect + " WHERE " + where
	}

	rows, err := obj.DB.Query(countSelect)

	if err != nil {
		return "", err
	}
	for rows.Next() {
		err = rows.Scan(&countRows)
		if err != nil {
			return "", err
		}
	}
	return countRows, err
}
func (obj MySQLObject) LastRow(table string) (string, error) {
	var countRows string
	countSelect := "SELECT max(id) FROM " + table

	rows, err := obj.DB.Query(countSelect)

	if err != nil {
		return "", err
	}
	for rows.Next() {
		err = rows.Scan(&countRows)
		if err != nil {
			return "", err
		}
	}
	return countRows, err
}
func (obj MySQLObject) Close() {
	obj.DB.Close()
}
func (obj MySQLObject) SelectToOne(value interface{}, from string, where string, additionalTag string) (int, error) {
	count := 0
	endSimbol := ","
	selectString := "SELECT "
	t := reflect.ValueOf(value).Elem()
	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct)   // Allocate enough values
	valsInt := make([]sql.NullInt64, lenStruct)       // Allocate enough values
	valsFloat64 := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	ints := make([]interface{}, lenStruct)            // Allocate middle values
	intsBool := make([]bool, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable string
	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field

		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(t.Type().Field(npc).Name)
			}

		} else {
			sqlVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if npc == lenStruct {
			endSimbol = ""

			if selectString[len(selectString)-1] == ',' {
				selectString = selectString[:len(selectString)-1] + endSimbol
			}
		}

		if sqlVariable == "" {
			l = l - 1
			valsInt = valsInt[:l]
			valsFloat64 = valsFloat64[:l]
			valsString = valsString[:l]
			ints = ints[:l]
		} else {
			selectString = selectString + sqlVariable + endSimbol

			typeOfValues := t.Type().Field(npc).Type.Name()
			intsBool[npc] = true
			//fmt.Println("Type: ",typeOfValues) //
			switch typeOfValues {
			case "string":
				ints[k] = &valsString[k] // Copy references into the middle values slice
			case "int":
				ints[k] = &valsInt[k]
			case "float32":
				ints[k] = &valsFloat64[k]
			case "float64":
				ints[k] = &valsFloat64[k]
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return 0, fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
			k += 1
		}
	}
	if where != "" {
		selectString = selectString[:len(selectString)-1] + " FROM " + from + " WHERE " + where
	} else {
		selectString = selectString[:len(selectString)-1] + " FROM " + from
	}
	rows, err := obj.DB.Query(selectString)

	if err != nil {
		return count, err
	} else {
		for rows.Next() {
			count += 1
			err = rows.Scan(ints...)
			if err != nil {
				return count, err
			}
			k = 0
			for i := 0; i < lenStruct; i++ {
				v := t.Field(i)
				if intsBool[i] {
					switch ints[k].(type) {
					case *sql.NullString:
						//fmt.Println("Type: string", )
						if valsString[k].Valid {
							v.SetString(valsString[k].String) // Copy not NULL values into variables of struct
						} else {
							v.SetString("")
						}
					case *sql.NullInt64:
						//fmt.Println("Type: int", )
						if valsInt[k].Valid {
							v.SetInt(valsInt[k].Int64)
						} else {
							v.SetInt(0)
						}
					case *sql.NullFloat64:
						//fmt.Println("Type: float", )
						if valsFloat64[k].Valid {
							v.SetFloat(valsFloat64[k].Float64)
						} else {
							v.SetFloat(0)
						}
					default:
						//log.Printf("Error UNKNOWN type: %T", ints[i])
						return 0, fmt.Errorf("Error UNKNOWN type: %T\n", ints[i])
					}
					k++
				}
			}
		}
	}
	return count, nil
}
func (obj MySQLObject) SelectToMany(value interface{}, from string, where string, limit string, additionalTag string) (interface{}, int, error) {
	count := 0
	endSimbol := ","
	selectString := "SELECT "
	t := reflect.TypeOf(value)
	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct)   // Allocate enough values
	valsInt := make([]sql.NullInt64, lenStruct)       // Allocate enough values
	valsFloat64 := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	ints := make([]interface{}, lenStruct)            // Allocate middle values
	intsBool := make([]bool, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable string
	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field

		if additionalTag == EmptyString {
			_, flag := t.Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(t.Field(npc).Name)
			}

		} else {
			sqlVariable = t.Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if npc == lenStruct {
			endSimbol = ""

			if selectString[len(selectString)-1] == ',' {
				selectString = selectString[:len(selectString)-1] + endSimbol
			}
		}

		if sqlVariable == "" {
			l = l - 1
			valsInt = valsInt[:l]
			valsFloat64 = valsFloat64[:l]
			valsString = valsString[:l]
			ints = ints[:l]
		} else {
			selectString = selectString + sqlVariable + endSimbol

			typeOfValues := t.Field(npc).Type.Name()
			intsBool[npc] = true
			//fmt.Println("Type: ",typeOfValues) //
			switch typeOfValues {
			case "string":
				ints[k] = &valsString[k] // Copy references into the middle values slice
			case "int":
				ints[k] = &valsInt[k]
			case "float32":
				ints[k] = &valsFloat64[k]
			case "float64":
				ints[k] = &valsFloat64[k]
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return nil, count, fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
			k += 1
		}
	}
	if where != "" {
		selectString = selectString[:len(selectString)-1] + " FROM " + from + " WHERE " + where
	} else {
		selectString = selectString[:len(selectString)-1] + " FROM " + from
	}
	//fmt.Println(selectString)
	rows, err := obj.DB.Query(selectString)

	if err != nil {
		return nil, count, err
	} else {
		arr := reflect.MakeSlice(reflect.SliceOf(t), 0, 0)
		val := reflect.New(t).Elem()
		for rows.Next() {
			count += 1
			err = rows.Scan(ints...)
			if err != nil {
				return nil, count, err
			}

			k = 0
			for i := 0; i < lenStruct; i++ {
				if intsBool[i] {
					v := val.Field(i)
					switch ints[k].(type) {
					case *sql.NullString:
						if valsString[k].Valid {
							v.SetString(valsString[k].String) // Copy not NULL values into variables of struct
						} else {
							v.SetString("")
						}
					case *sql.NullInt64:
						if valsInt[k].Valid {
							v.SetInt(valsInt[k].Int64)
						} else {
							v.SetInt(0)
						}
					case *sql.NullFloat64:
						if valsFloat64[k].Valid {
							v.SetFloat(valsFloat64[k].Float64)
						} else {
							v.SetFloat(0)
						}
					default:
						return nil, count, fmt.Errorf("Error UNKNOWN type: %T", ints[i])
					}
					k++
				}
			}
			arr = reflect.Append(arr, val)
		}
		return arr.Interface(), count, nil
	}
	return nil, count, nil
}
func (obj MySQLObject) Insert(value interface{}, table string, additionalTag string) error {
	insertValues := "INSERT INTO " + table + " ("
	valueQMStr := " VALUES ("
	t := reflect.ValueOf(value).Elem()

	lenStruct := t.NumField()
	values := make([]interface{}, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable string
	for npc := 0; npc < lenStruct; npc++ {
		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(t.Type().Field(npc).Name)
			}

		} else {
			sqlVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if sqlVariable == "" {
			l = l - 1
			values = values[:l]
		} else {
			insertValues = insertValues + sqlVariable + ","
			values[k] = t.Field(npc).Interface()
			k += 1

			valueQMStr = valueQMStr + "?," //for Mysql

		}
	}
	insertValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ")"
	//fmt.Println(insertValues)
	//fmt.Println(values)
	stmt, err := obj.DB.Prepare(insertValues)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err2 := stmt.Exec(values...)
	if err2 != nil {
		return err2
	}
	return nil
}
func (obj MySQLObject) Update(set interface{}, table string, where string, tagName string) error {
	var sqlVariable string
	updateStr := "UPDATE " + table
	setStr := " SET "

	if where != "" {
		where = " WHERE " + where
	}

	v := reflect.ValueOf(set)

	structS := v.Elem()
	lenStruct := structS.NumField()
	values := make([]interface{}, lenStruct)
	k := 0
	l := lenStruct
	for i := 0; i < lenStruct; i++ {
		if tagName == EmptyString {
			_, flag := structS.Type().Field(i).Tag.Lookup("sql")
			if flag {
				sqlVariable = structS.Type().Field(i).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(structS.Type().Field(i).Name)
			}
		} else {
			sqlVariable = structS.Type().Field(i).Tag.Get(tagName) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}
		if sqlVariable == "" {
			l = l - 1
			values = values[:l]
		} else {
			setStr = setStr + sqlVariable + "=?,"
			values[k] = structS.Field(i).Interface()
			k += 1
		}
	}
	updateStr = updateStr + setStr[:len(setStr)-1] + where

	//fmt.Println(updateStr)

	stmt, err := obj.DB.Prepare(updateStr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	//fmt.Println(values)
	_, err2 := stmt.Exec(values...)
	if err2 != nil {
		return err2
	}
	return nil
}
func (obj MySQLObject) InsertOrUpdate(value interface{}, table string, constrain string, tagName string) error {
	var sqlVariable string
	insertValues := "INSERT INTO " + table + " ("
	valueQMStr := " VALUES ("
	updateStr := " ON DUPLICATE KEY UPDATE "

	v := reflect.ValueOf(value)

	structS := v.Elem()
	lenStruct := structS.NumField()
	values := make([]interface{}, lenStruct)

	k := 0
	l := lenStruct

	for i := 0; i < lenStruct; i++ {
		if tagName == EmptyString {
			_, flag := structS.Type().Field(i).Tag.Lookup("sql")
			if flag {
				sqlVariable = structS.Type().Field(i).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(structS.Type().Field(i).Name)
			}

		} else {
			sqlVariable = structS.Type().Field(i).Tag.Get(tagName) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if sqlVariable == EmptyString {
			l = l - 1
			values = values[:l]
		} else {
			insertValues = insertValues + sqlVariable + ","

			valueQMStr = valueQMStr + "?,"
			updateStr = updateStr + sqlVariable + "=?,"

			values[k] = structS.Field(i).Interface()
			k += 1
		}
	}
	for _, r := range values {
		values = append(values, r)
	}
	var insertUpdateValues string

	insertUpdateValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ")" + updateStr[:len(updateStr)-1]

	//fmt.Println(insertUpdateValues)

	//
	stmt, err := obj.DB.Prepare(insertUpdateValues)
	if err != nil {
		return err
	}
	defer stmt.Close()
	//fmt.Println(values)
	_, err2 := stmt.Exec(values...)
	if err2 != nil {
		return err2
	}
	return nil
}
func (obj MySQLObject) Delete(table string, where string) error {
	stmt, err := obj.DB.Prepare("delete from " + table + "where name=?")
	_, err = stmt.Exec(where)
	return err
}

/*
func(obj MySQLObject) SelectMap(from string, where string) (map[string]interface{}, error) {

		//var fieldsExpans [][]string
		cols, err := rows.Columns()
		checkErr(err, point + " => Columns")
		pointers := make([]interface{}, len(cols))
		container := make([]sql.RawBytes, len(cols))
		fieldsExpans := make(map[string]string)
		for i := range pointers {
			pointers[i] = &container[i]
		}
		for rows.Next() {
			err = rows.Scan(pointers...)
			checkErr(err, point + " => Scan in loop")
			for i, col := range container {
				// Here we can check if the value is nil (NULL value)
				if col == nil {
					fieldsExpans[cols[i]] = "NULL"
				} else {
					fieldsExpans[cols[i]] = string(col)
				}
			}
		}

		if err = rows.Close(); err != nil {
			// but what should we do if there's an error?
			return nil,err
		}
		return fieldsExpans, nil
	}
	if rows.Next() {
		err = rows.Scan(&(phoneData.vlan), &(phoneData.name), &(phoneData.secret), &(phoneData.mhplogin), &(phoneData.expansemod))
		checkErr(err, point+" => Scan ")
		if phoneData.expansemod {
			mysqlconf.Table = mysqlconf.TableEx //"expansmod"
			Logger(logging, 0, filterMsg, "Recursive call for 'dbSelectData' => '%s'", mhplogin)
			phoneData.expansFiels, _ = dbSelectData(mysqlconf, mhplogin, "mhplogin", true)
		} else {
			phoneData.expansFiels = nil
		}
	}
	if err = rows.Close(); err != nil {
		// but what should we do if there's an error?
		checkErr(err, point+" => rows.Close loop")
	}
	return nil, &phoneData
}
*/
