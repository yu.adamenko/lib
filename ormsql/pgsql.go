// Copyright (C) 2018 Adamenko Yuriy <yu.adamenko@gmail.com>
//

package ormsql

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"reflect"
	"strconv"
	"strings"
)

func (obj PgSQLObject) CountRows(table string, where string) (string, error) {
	var countRows string
	countSelect := "SELECT COUNT(*) FROM " + table
	if where != "" {
		countSelect = countSelect + " WHERE " + where
	}
	rows, err := obj.DB.Query(countSelect)

	if err != nil {
		return "", err
	}
	for rows.Next() {
		err = rows.Scan(&countRows)
		if err != nil {
			return "", err
		}
	}
	return countRows, err
}
func (obj PgSQLObject) LastRow(table string) (string, error) {
	var countRows string
	countSelect := "SELECT max(id) FROM " + table

	rows, err := obj.DB.Query(countSelect)

	if err != nil {
		return "", err
	}
	for rows.Next() {
		err = rows.Scan(&countRows)
		if err != nil {
			return "", err
		}
	}
	return countRows, err
}
func (obj PgSQLObject) Close() error {
	return obj.DB.Close()
}
func (obj PgSQLObject) SelectToOne(value interface{}, from string, where string, additionalTag string) (int, error) {

	count := 0
	endSimbol := "\",\""
	selectString := "SELECT \""
	t := reflect.ValueOf(value).Elem()
	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct)   // Allocate enough values
	valsInt64 := make([]sql.NullInt64, lenStruct)     // Allocate enough values
	valsFloat64 := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	valsUUID := make([]NullUUID, lenStruct)           // Allocate enough values
	valsBool := make([]sql.NullBool, lenStruct)       // Allocate enough values
	ints := make([]interface{}, lenStruct)            // Allocate middle values
	intsBool := make([]bool, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable string
	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field

		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = t.Type().Field(npc).Name
			}

		} else {
			sqlVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if npc == lenStruct {
			endSimbol = ""
			if selectString[len(selectString)-2] == ',' {
				selectString = selectString[:len(selectString)-2]
			}
		}

		if sqlVariable == "" {
			l = l - 1
			valsInt64 = valsInt64[:l]
			valsBool = valsBool[:l] // ????
			valsFloat64 = valsFloat64[:l]
			valsString = valsString[:l]
			valsUUID = valsUUID[:l]
			ints = ints[:l]
		} else {
			selectString = selectString + sqlVariable + endSimbol

			typeOfValues := t.Type().Field(npc).Type.Name()
			intsBool[npc] = true
			//fmt.Println("Type: ",typeOfValues) //
			switch typeOfValues {
			case "string":
				ints[k] = &valsString[k] // Copy references into the middle values slice
			case "int64":
				ints[k] = &valsInt64[k]
			case "float32":
				ints[k] = &valsFloat64[k]
			case "float64":
				ints[k] = &valsFloat64[k]
			case "bool":
				ints[k] = &valsBool[k]
			case "UUID":
				ints[k] = &valsUUID[k]
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return 0, fmt.Errorf("Error [1] UNKNOWN type: %s\n", typeOfValues)
			}
			k += 1
		}
	}
	if where != "" {
		selectString = selectString[:len(selectString)-2] + " FROM " + from + " WHERE " + where
	} else {
		selectString = selectString[:len(selectString)-2] + " FROM " + from
	}
	logging.Debug(8).Printf("Select string : [ %s ] \n", selectString)
	//log.Debug(8).Printf("Select string : [ %s ] \n", selectString)
	//fmt.Println(selectString)
	rows, err := obj.DB.Query(selectString)

	if err != nil {
		return count, err
	} else {
		for rows.Next() {
			count += 1
			err = rows.Scan(ints...)
			if err != nil {
				return count, err
			}
			k = 0
			for i := 0; i < lenStruct; i++ {
				v := t.Field(i)
				if intsBool[i] {
					switch ints[k].(type) {
					case *sql.NullString:
						//fmt.Println("Type: string", )
						if valsString[k].Valid {
							v.SetString(valsString[k].String) // Copy not NULL values into variables of struct
						} else {
							v.SetString("")
						}
					case *sql.NullInt64:
						//fmt.Println("Type: int", )
						if valsInt64[k].Valid {
							v.SetInt(valsInt64[k].Int64)
						} else {
							v.SetInt(0)
						}
					case *sql.NullFloat64:
						//fmt.Println("Type: float", )
						if valsFloat64[k].Valid {
							v.SetFloat(valsFloat64[k].Float64)
						} else {
							v.SetFloat(0)
						}
					case *sql.NullBool:
						//fmt.Println("Type: float", )
						if valsBool[k].Valid {
							v.SetBool(valsBool[k].Bool)
						} else {
							v.SetBool(false)
						}
					case *NullUUID:
						if valsUUID[k].Valid {
							reflect.Copy(v, reflect.ValueOf(valsUUID[k].UUID[:]))
						} else {
							NilUUID := [16]byte{}
							reflect.Copy(v, reflect.ValueOf(NilUUID[:]))
						}
					default:
						return 0, fmt.Errorf("Error [2] UNKNOWN type: %T\n", ints[i])
					}
					k++
				}
			}
		}
	}
	return count, nil
}
//
func (obj PgSQLObject) SelectToMany(value interface{}, from string, where string, limit string, additionalTag string) (interface{}, int, error) {
	count := 0
	endSimbol := ","
	selectString := "SELECT "
	t := reflect.TypeOf(value)
	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct)   // Allocate enough values
	valsInt := make([]sql.NullInt64, lenStruct)       // Allocate enough values
	valsFloat64 := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	ints := make([]interface{}, lenStruct)            // Allocate middle values
	intsBool := make([]bool, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable string
	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field

		if additionalTag == EmptyString {
			_, flag := t.Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(t.Field(npc).Name)
			}

		} else {
			sqlVariable = t.Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if npc == lenStruct {
			endSimbol = ""

			if selectString[len(selectString)-1] == ',' {
				selectString = selectString[:len(selectString)-1] + endSimbol
			}
		}

		if sqlVariable == "" {
			l = l - 1
			valsInt = valsInt[:l]
			valsFloat64 = valsFloat64[:l]
			valsString = valsString[:l]
			ints = ints[:l]
		} else {
			selectString = selectString + sqlVariable + endSimbol

			typeOfValues := t.Field(npc).Type.Name()
			intsBool[npc] = true
			//fmt.Println("Type: ",typeOfValues) //
			switch typeOfValues {
			case "string":
				ints[k] = &valsString[k] // Copy references into the middle values slice
			case "int":
				ints[k] = &valsInt[k]
			case "float32":
				ints[k] = &valsFloat64[k]
			case "float64":
				ints[k] = &valsFloat64[k]
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return nil, count, fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
			k += 1
		}
	}
	if where != "" {
		selectString = selectString[:len(selectString)-1] + " FROM " + from + " WHERE " + where
	} else {
		selectString = selectString[:len(selectString)-1] + " FROM " + from
	}
	//fmt.Println(selectString)
	rows, err := obj.DB.Query(selectString)

	if err != nil {
		return nil, count, err
	} else {
		arr := reflect.MakeSlice(reflect.SliceOf(t), 0, 0)
		val := reflect.New(t).Elem()
		for rows.Next() {
			count += 1
			err = rows.Scan(ints...)
			if err != nil {
				return nil, count, err
			}

			k = 0
			for i := 0; i < lenStruct; i++ {
				if intsBool[i] {
					v := val.Field(i)
					switch ints[k].(type) {
					case *sql.NullString:
						if valsString[k].Valid {
							v.SetString(valsString[k].String) // Copy not NULL values into variables of struct
						} else {
							v.SetString("")
						}
					case *sql.NullInt64:
						if valsInt[k].Valid {
							v.SetInt(valsInt[k].Int64)
						} else {
							v.SetInt(0)
						}
					case *sql.NullFloat64:
						if valsFloat64[k].Valid {
							v.SetFloat(valsFloat64[k].Float64)
						} else {
							v.SetFloat(0)
						}
					default:
						return nil, count, fmt.Errorf("error UNKNOWN type: %T", ints[i])
					}
					k++
				}
			}
			arr = reflect.Append(arr, val)
		}
		return arr.Interface(), count, nil
	}
}
func (obj PgSQLObject) Insert(value interface{}, table string, additionalTag string) error {
	insertValues := "INSERT INTO " + table + " (\""
	valueQMStr := " VALUES ("
	t := reflect.ValueOf(value).Elem()

	lenStruct := t.NumField()
	values := make([]interface{}, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable string
	for npc := 0; npc < lenStruct; npc++ {
		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = t.Type().Field(npc).Name
			}

		} else {
			sqlVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if sqlVariable == "" {
			l = l - 1
			values = values[:l]
		} else {
			insertValues = insertValues + sqlVariable + "\",\""
			values[k] = t.Field(npc).Interface()
			k += 1
			valueQMStr = valueQMStr + "$" + strconv.Itoa(k) + ","
		}
	}
	insertValues = insertValues[:len(insertValues)-2] + ")" + valueQMStr[:len(valueQMStr)-1] + ")"
	//log.Debug(8).Printf("%s\n", insertValues)
	//log.Debug(8).Println(values)

	stmt, err := obj.DB.Prepare(insertValues)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err2 := stmt.Exec(values...)
	if err2 != nil {
		return err2
	}
	return nil
}
func (obj PgSQLObject) InsertDoNothing(value interface{}, table string, conflict string, additionalTag string) error {
	insertValues := "INSERT INTO " + table + " (\""
	valueQMStr := " VALUES ("
	t := reflect.ValueOf(value).Elem()

	lenStruct := t.NumField()
	values := make([]interface{}, lenStruct)
	k := 0
	l := lenStruct
	var sqlVariable, sqlVars string
	for npc := 0; npc < lenStruct; npc++ {
		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlVariable = t.Type().Field(npc).Name
			}

		} else {
			sqlVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if sqlVariable == "" {
			l = l - 1
			values = values[:l]
		} else {
			sqlVars = sqlVars + sqlVariable + "\",\""
			//insertValues = insertValues + sqlVariable + "\",\""
			values[k] = t.Field(npc).Interface()
			k += 1
			valueQMStr = valueQMStr + "$" + strconv.Itoa(k) + ","
		}
	}
	insertValues = insertValues + sqlVars[:len(sqlVars)-2] + ")" + valueQMStr[:len(valueQMStr)-1] + ")" + " ON CONFLICT (\"" + conflict + "\") DO NOTHING"
	logging.Debug(8).Printf("%s\n", insertValues)
	logging.Debug(8).Println(values)

	stmt, err := obj.DB.Prepare(insertValues)
	if err != nil {
		return err
	}
	defer stmt.Close()

	//values = append(values, values)
	_, err2 := stmt.Exec(values...)
	if err2 != nil {
		return err2
	}
	return nil
}
func (obj PgSQLObject) InsertReturn(value interface{}, table string, additionalTag string, returning interface{}, additionalRetTag string) error {
	insertValues := "INSERT INTO " + table + " ("
	valueQMStr := " VALUES ("
	t := reflect.ValueOf(value).Elem()

	lenStruct := t.NumField()
	values := make([]interface{}, lenStruct)
	k := 0
	l := lenStruct
	var sqlInsertVariable string
	for npc := 0; npc < lenStruct; npc++ {
		if additionalTag == EmptyString {
			_, flag := t.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlInsertVariable = t.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlInsertVariable = strings.ToLower(t.Type().Field(npc).Name)
			}

		} else {
			sqlInsertVariable = t.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlInsertVariable, ",") {
			sqlInsertVariables := strings.SplitAfter(sqlInsertVariable, ",")
			sqlInsertVariable = sqlInsertVariables[0][:len(sqlInsertVariables[0])-1]
		}

		if sqlInsertVariable == "" {
			l = l - 1
			values = values[:l]
		} else {
			insertValues = insertValues + sqlInsertVariable + ","
			values[k] = t.Field(npc).Interface()
			k += 1
			typeOfValues := t.Type().Field(npc).Type.Name()
			switch typeOfValues {
			case "string":
				valueQMStr = valueQMStr + "'" + t.Field(npc).String() + "',"
			case "int":
				valueQMStr = valueQMStr + strconv.Itoa(int(t.Field(npc).Int())) + ","
			case "int32":
				valueQMStr = valueQMStr + strconv.Itoa(int(t.Field(npc).Int())) + ","
			case "int64":
				valueQMStr = valueQMStr + strconv.Itoa(int(t.Field(npc).Int())) + ","
			case "float32":
				valueQMStr = valueQMStr + fmt.Sprintf("%f", t.Field(npc).Float()) + ","
			case "float64":
				valueQMStr = valueQMStr + fmt.Sprintf("%f", t.Field(npc).Float()) + ","
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
		}
	}
	insertValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ") RETURNING "

	rt := reflect.ValueOf(returning).Elem()
	lenRetStruct := rt.NumField()

	valsString := make([]sql.NullString, lenRetStruct)   // Allocate enough values
	valsInt64 := make([]sql.NullInt64, lenRetStruct)     // Allocate enough values
	valsFloat64 := make([]sql.NullFloat64, lenRetStruct) // Allocate enough values
	ints := make([]interface{}, lenRetStruct)            // Allocate middle values
	intsBool := make([]bool, lenRetStruct)

	count := 0
	k = 0
	//l := lenRetStruct
	var sqlRetVariable, sqlRetString string
	for npc := 0; npc < lenRetStruct; npc++ { // iterates through every struct type field

		if additionalRetTag == EmptyString {
			_, flag := rt.Type().Field(npc).Tag.Lookup("sql")
			if flag {
				sqlRetVariable = rt.Type().Field(npc).Tag.Get("sql")
			} else {
				sqlRetVariable = strings.ToLower(rt.Type().Field(npc).Name)
			}

		} else {
			sqlRetVariable = rt.Type().Field(npc).Tag.Get(additionalTag) // returns the tag string
		}

		if strings.Contains(sqlRetVariable, ",") {
			sqlRetVariables := strings.SplitAfter(sqlRetVariable, ",")
			sqlRetVariable = sqlRetVariables[0][:len(sqlRetVariables[0])-1]
		}

		if sqlRetVariable == "" {
			l = l - 1
			valsInt64 = valsInt64[:l]
			valsFloat64 = valsFloat64[:l]
			valsString = valsString[:l]
			ints = ints[:l]
		} else {

			typeOfValues := rt.Type().Field(npc).Type.Name()
			intsBool[npc] = true
			//fmt.Println("Type: ",typeOfValues) //
			switch typeOfValues {
			case "string":
				ints[k] = &valsString[k] // Copy references into the middle values slice
			case "int64":
				ints[k] = &valsInt64[k]
			case "float32":
				ints[k] = &valsFloat64[k]
			case "float64":
				ints[k] = &valsFloat64[k]
			default:
				//fmt.Printf("Error UNKNOWN type: %s \n", typeOfValues)
				return fmt.Errorf("Error UNKNOWN type: %s\n", typeOfValues)
			}
			k += 1
		}
		sqlRetString = sqlRetString + "," + sqlRetVariable
	}

	sqlString := insertValues + sqlRetString[1:]

	rows, err := obj.DB.Query(sqlString)

	if err != nil {
		return err
	} else {
		for rows.Next() {
			count += 1
			err = rows.Scan(ints...)
			if err != nil {
				return err
			}
			k = 0
			for i := 0; i < lenRetStruct; i++ {
				v := rt.Field(i)
				if intsBool[i] {
					switch ints[k].(type) {
					case *sql.NullString:
						//fmt.Println("Type: string", )
						if valsString[k].Valid {
							v.SetString(valsString[k].String) // Copy not NULL values into variables of struct
						} else {
							v.SetString("")
						}
					case *sql.NullInt64:
						//fmt.Println("Type: int", )
						if valsInt64[k].Valid {
							v.SetInt(valsInt64[k].Int64)
						} else {
							v.SetInt(0)
						}
					case *sql.NullFloat64:
						//fmt.Println("Type: float", )
						if valsFloat64[k].Valid {
							v.SetFloat(valsFloat64[k].Float64)
						} else {
							v.SetFloat(0)
						}
					default:
						//log.Printf("Error UNKNOWN type: %T", ints[i])
						return fmt.Errorf("Error UNKNOWN type: %T\n", ints[i])
					}
					k++
				}
			}
		}
	}
	return nil
}
func (obj PgSQLObject) InsertOrUpdate(value interface{}, table string, conflict string, condition string, tagInsert string, tagUpdate string) error {
	var sqlInsVariable string
	var sqlUpdVariable string

	insertValues := "INSERT INTO " + table + " ("
	valueQMStr := " VALUES ("
	var updateStrVar string
	updateStr := " ON CONFLICT "

	v := reflect.ValueOf(value)

	structS := v.Elem()
	lenStruct := structS.NumField()
	values := make([]interface{}, lenStruct)

	k := 0
	l := lenStruct

	for i := 0; i < lenStruct; i++ {
		if tagInsert == EmptyString {
			_, flag := structS.Type().Field(i).Tag.Lookup("sql")
			if flag {
				sqlInsVariable = structS.Type().Field(i).Tag.Get("sql")
			} else {
				sqlInsVariable = strings.ToLower(structS.Type().Field(i).Name)
			}

		} else {
			sqlInsVariable = structS.Type().Field(i).Tag.Get(tagInsert) // returns the tag string
		}
		if tagUpdate != EmptyString {
			sqlUpdVariable = structS.Type().Field(i).Tag.Get(tagUpdate)
		}

		if strings.Contains(sqlInsVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlInsVariable, ",")
			sqlInsVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if strings.Contains(sqlUpdVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlUpdVariable, ",")
			sqlUpdVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}

		if sqlInsVariable == EmptyString && sqlUpdVariable == EmptyString {
			l = l - 1
			values = values[:l]
		} else {
			values[k] = structS.Field(i).Interface()
			k += 1
		}

		if sqlInsVariable != EmptyString {
			insertValues = insertValues + "\"" + sqlInsVariable + "\","
			valueQMStr = valueQMStr + "$" + strconv.Itoa(i+1) + ","
		}

		if sqlUpdVariable != EmptyString {
			updateStrVar = updateStrVar + sqlUpdVariable + "=$" + strconv.Itoa(i+1) + ","
		}
	}

	var insertUpdateValues string

	insertUpdateValues = insertValues[:len(insertValues)-1] + ")" + valueQMStr[:len(valueQMStr)-1] + ")" + updateStr + "(\"" + conflict + "\")" + " DO UPDATE SET " + updateStrVar[:len(updateStrVar)-1] + " " + condition
	logging.Debug(8).Printf("%s\n", insertUpdateValues)
	logging.Debug(8).Println(values)
	stmt, err := obj.DB.Prepare(insertUpdateValues)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err2 := stmt.Exec(values...)
	return err2
}
func (obj PgSQLObject) Update(set interface{}, table string, where string, tagName string) error {
	var sqlVariable string
	updateStr := "UPDATE " + table
	setStr := " SET "

	if where != "" {
		where = " WHERE " + where
	}

	v := reflect.ValueOf(set)

	structS := v.Elem()
	lenStruct := structS.NumField()
	values := make([]interface{}, lenStruct)
	k := 0
	l := lenStruct
	for i := 0; i < lenStruct; i++ {
		if tagName == EmptyString {
			_, flag := structS.Type().Field(i).Tag.Lookup("sql")
			if flag {
				sqlVariable = structS.Type().Field(i).Tag.Get("sql")
			} else {
				sqlVariable = strings.ToLower(structS.Type().Field(i).Name)
			}
		} else {
			sqlVariable = structS.Type().Field(i).Tag.Get(tagName) // returns the tag string
		}
		if strings.Contains(sqlVariable, ",") {
			sqlVariables := strings.SplitAfter(sqlVariable, ",")
			sqlVariable = sqlVariables[0][:len(sqlVariables[0])-1]
		}
		if sqlVariable == "" {
			l = l - 1
			values = values[:l]
		} else {
			setStr = setStr + "\"" + sqlVariable + "\"=$" + strconv.Itoa(k+1) + ","
			values[k] = structS.Field(i).Interface()
			k += 1
		}
	}
	updateStr = updateStr + setStr[:len(setStr)-1] + where

	logging.Debug(8).Printf("Update string : [ %s ] \n", updateStr)

	stmt, err := obj.DB.Prepare(updateStr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	fmt.Println(values)
	_, err2 := stmt.Exec(values...)
	if err2 != nil {
		return err2
	}
	return nil
}
func (obj PgSQLObject) Delete(table string, where string) error {
	stmt, err := obj.DB.Prepare("delete from " + table + "where name=?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(where)
	return err
}

/*
func(obj SQLObject) SelectMap(from string, where string) (map[string]interface{}, error) {

		//var fieldsExpans [][]string
		cols, err := rows.Columns()
		checkErr(err, point + " => Columns")
		pointers := make([]interface{}, len(cols))
		container := make([]sql.RawBytes, len(cols))
		fieldsExpans := make(map[string]string)
		for i := range pointers {
			pointers[i] = &container[i]
		}
		for rows.Next() {
			err = rows.Scan(pointers...)
			checkErr(err, point + " => Scan in loop")
			for i, col := range container {
				// Here we can check if the value is nil (NULL value)
				if col == nil {
					fieldsExpans[cols[i]] = "NULL"
				} else {
					fieldsExpans[cols[i]] = string(col)
				}
			}
		}

		if err = rows.Close(); err != nil {
			// but what should we do if there's an error?
			return nil,err
		}
		return fieldsExpans, nil
	}
	if rows.Next() {
		err = rows.Scan(&(phoneData.vlan), &(phoneData.name), &(phoneData.secret), &(phoneData.mhplogin), &(phoneData.expansemod))
		checkErr(err, point+" => Scan ")
		if phoneData.expansemod {
			mysqlconf.Table = mysqlconf.TableEx //"expansmod"
			Logger(logging, 0, filterMsg, "Recursive call for 'dbSelectData' => '%s'", mhplogin)
			phoneData.expansFiels, _ = dbSelectData(mysqlconf, mhplogin, "mhplogin", true)
		} else {
			phoneData.expansFiels = nil
		}
	}
	if err = rows.Close(); err != nil {
		// but what should we do if there's an error?
		checkErr(err, point+" => rows.Close loop")
	}
	return nil, &phoneData
}
*/
